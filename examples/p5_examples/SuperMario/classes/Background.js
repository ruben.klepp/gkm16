function Background() {
	var posX = 0;
	
	this.draw = function () {
		posX = (posX - 1) % width;
		image(images.background, posX, 0);
	}
}
