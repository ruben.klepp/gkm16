'use strict';

var imgBackground;
var imagesPlayer = [];
var players = [];
var items = [];
var imgItem;
var movingStrategies = [mouseMover, potiMover];
var distCollission = 50;

//Arduino
var b = p5.board('COM4', 'arduino'); //ggf. anpassen!
var poti;
var button;

function preload() {
    imgBackground = loadImage("assets/background.jpg");
    imagesPlayer.push(loadImage("assets/player1.jpg"));
    imagesPlayer.push(loadImage("assets/player2.jpg"));
    imagesPlayer.push(loadImage("assets/player3.jpg"));
    imgItem = loadImage("assets/item.jpg");
    console.log("end of preload");
}
function setup() {
    createCanvas(800, 400);
	//Arduino
	poti = b.pin(0, 'VRES'); //an A0 angeschlossen
	poti.read(); 
	poti.range([0, width]);
  
	button = b.pin(8, 'BUTTON'); //button an D8 angeschlossen
	button.pressed(spawnItem);
	button.read(); 
	
	//Spieler initialisieren
    background(imgBackground);
    for (var i = 0; i < Math.min(imagesPlayer.length, movingStrategies.length); i++)
        players.push(new Player(i, imagesPlayer[i], movingStrategies[i]));    
    console.log("end of setup");
}

function draw() {
    background(imgBackground);
    for (var i = 0; i < players.length; i++) {
        //check collection of items
        for (var k = 0; k < items.length; k++) {
            var d = distFixed(items[k].position, players[i].position);
            if (d < distCollission) {
                players[i].scored();
                items[k].eaten();
            }
            items[k].draw();
        }
        players[i].update();
        players[i].draw();
    }
}

function distFixed(v1, v2) {
    return Math.sqrt((v2.x - v1.x) * (v2.x - v1.x) + (v2.y - v1.y) * (v2.y - v1.y));
}

function mouseMover(x, y) {
    return {x: mouseX, y: mouseY};
}

function potiMover(x, yOld) {
    return {x: poti.val, y: yOld};
}

function spawnItem(){
	items.push(new Item());
}