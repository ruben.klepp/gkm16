'use strict';

function Player (id, img, movingStrategy) {
  this.id = id;
  this.score = 0;
  this.img = img;
  this.lastPosition = createVector(0,0);
  this.movingStrategy = movingStrategy;
  this.position = createVector(random(0,width), random(0, height));
}

Player.prototype.update = function(){
  this.lastPosition = this.position.copy();
  var newPos = this.movingStrategy(this.lastPosition.x, this.lastPosition.y);
  this.position.x = newPos.x;
  this.position.y = newPos.y;
}
 
Player.prototype.draw = function(){
    image(this.img, this.position.x, this.position.y);
}

Player.prototype.scored = function() {
    //console.log("player " + this.id + "scored");
    this.score++;
    var nodeId = "score-" + this.id;
    var node = document.getElementById(nodeId);
    if (node) node.innerHTML = this.score;
    else console.warn("node with id " + nodeId + " not found");
}